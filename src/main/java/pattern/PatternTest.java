package pattern;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternTest {


    public static void main(String[] args) {

        Pattern pattern = Pattern.compile("^[A-Z]|[a-z]$");
        Matcher matcher = pattern.matcher("This is true");

        while (matcher.find()) {
            System.out.println(matcher.start() + " " + matcher.end());

        }

        String string = "The best thing is you";
        String [] splitted = string.split("([Th]he|[Yy]ou)");

        Arrays.stream(splitted).forEach(System.out::println);

    }

}
