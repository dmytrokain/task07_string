package pattern;

public class Vowels {

    public static void main(String[] args) {

        String text = "The best thing is something good";

        System.out.println(text.replaceAll("[Auoeiauoei]", "_"));
    }
}
