package TextTask;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class TextParser {

    private Logger logger;
    private Pattern pattern;
    private Matcher matcher;
    private BufferedReader reader;
    private StringBuilder stringBuilder;
    private ArrayList<String> strings;
    private String text;
    private ArrayList<String> sentence;

    public TextParser(String path) {
        logger =  Logger.getLogger(TextParser.class.getName());
        strings = new ArrayList<>();
        stringBuilder = new StringBuilder();
        sentence = new ArrayList<>();

        try {

            reader = new BufferedReader(
                            new FileReader(new File(path)));
            logger.info("Opened");

            reader.lines().forEach(strings::add);

            text = strings.toString();

            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFound");
        } catch (IOException b) {}

    }

    public void printText() {
        System.out.println(text);
    }

    public String changeTabsToSpace() {
        return text.replaceAll(" ", "");
    }

    public void sentencesDivide() {
        pattern = Pattern.compile("[^\\[][\\.?!]");
        matcher = pattern.matcher(text);

        int start = 0;
        while (matcher.find()) {
            System.out.println(text.substring(start, matcher.start()+1).trim());
            sentence.add(text.substring(start, matcher.start()+1).trim());
        }

        System.out.println(sentence);
    }



    public static void main(String[] args) {
        TextParser parser = new TextParser("/home/dmytro/IdeaProjects/" +
                "task07_String/src/main/java/TextTask/text");

        parser.sentencesDivide();
    }

}
