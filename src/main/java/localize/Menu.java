package localize;

import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Menu {

    public static String concat(String... args) {
        String string = "";

        for(String sr: args) {
            string = string.concat(sr);
        }

        return string;
    }

    public static void printMenu(String localize) {

        ResourceBundle resourceBundle
                = ResourceBundle.getBundle(localize);

        LinkedList<String> list = new LinkedList<String>();


        list.add((String) resourceBundle.getObject("main"));
        list.add((String) resourceBundle.getObject("change"));
        list.add((String) resourceBundle.getObject("exit"));

        list.stream().forEach(System.out::println);
    }

    public static String changed(boolean num) {
        if(num) {
            return "localizate_en";
        } else {
            return "localizate_ua";
        }
    }

    static int num = 0;

    public static void run() {
        Scanner scanner = new Scanner(System.in);

        if(scanner.nextInt() == 1){
            if(num % 2 == 0){
                printMenu(changed(true));
                num++;
                run();
            }
            else {
                printMenu(changed(false));
                num++;
                run();
            }
        } else {
            return;
        }
        scanner.close();
    }


    public static void main(String[] args) {
        printMenu("localizate_ua");
        run();
    }
}
